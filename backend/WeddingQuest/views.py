import datetime

from django.contrib.auth.decorators import login_required
from django.contrib.auth import authenticate, login as auth_login, logout as auth_logout
from django.contrib.auth.models import User
from django.shortcuts import render, redirect, get_object_or_404
from django.http import HttpResponse, Http404, HttpResponseBadRequest
from .forms import UserCreationForm, LockedNodeForm, OpenNodeForm, LoginForm
from .models import (
    Node, NodeUser, Answer, UserAnswerGuess, AltErrorMsg
)


def distance(a, b):
    n, m = len(a), len(b)
    if n > m:
        # Make sure n <= m, to use O(min(n, m)) space
        a, b = b, a
        n, m = m, n
    current_row = range(n + 1)  # Keep current and previous row, not entire matrix
    for i in range(1, m + 1):
        previous_row, current_row = current_row, [i] + [0] * n
        for j in range(1, n + 1):
            add, delete, change = previous_row[j] + 1, current_row[j - 1] + 1, previous_row[j - 1]
            if a[j - 1] != b[i - 1]:
                change += 1
            current_row[j] = min(add, delete, change)
    return current_row[n]


def get_node(node_id: object) -> object:
    try:
        _node = Node.objects.get(id=node_id, is_active=True)
    except Node.DoesNotExist:
        _node = None
    return _node


def get_level_nodes_header(_node):
    result = []
    if _node.is_start:
        nodelist = [_node]
    else:
        level_nodes = Node.objects.filter(parent=_node.parent, is_active=True).order_by('pk')
        alt_nodes = level_nodes.filter(type='ALT')
        main_node = level_nodes.get(type='MAIN')
        nodelist = [alt_nodes[0], main_node, alt_nodes[1]]
    for level_node in nodelist:
        result.append({
            'id': level_node.id,
            'icon': level_node.icon.url,
            'class': 'main-icon' if level_node.id == _node.id else 'alt-icon'
        })
    return result


def login(request):
    context = {}
    if request.method == 'POST':
        form = LoginForm(request.POST)
        if form.is_valid():
            cleaned_data = form.cleaned_data
            user = authenticate(username=cleaned_data['username'], password=cleaned_data['password'])
            if user is not None:
                if user.is_active:
                    auth_login(request, user)
                    return redirect('home')
                else:
                    context['error_msg'] = 'Аккаунт отключен'
            else:
                context['error_msg'] = 'Неправильный логин или пароль'
    else:
        form = LoginForm()
    context['form'] = form
    return render(request, 'registration/login.html', context)


def logout(request):
    auth_logout(request)
    return render(request, 'registration/logged_out.html')


def signup(request):
    context = {}
    if request.method == 'POST':
        form = UserCreationForm(request.POST)
        if form.is_valid():
            cleaned_data = form.cleaned_data
            existing_users = User.objects.filter(username=cleaned_data['username'].lower())
            if existing_users.count() == 0:
                if cleaned_data['password1'] == cleaned_data['password2']:
                    user = User.objects.create_user(
                        username=cleaned_data['username'],
                        password=cleaned_data['password1']
                    )
                    auth_login(request, user)
                    for _node in Node.objects.all():
                        node_user = NodeUser(
                            node=_node,
                            user=user,
                        )
                        node_user.save()
                    return redirect('home')
                else:
                    context['error_msg'] = 'Пароли не совпадают'
            else:
                context['error_msg'] = 'Команда с таким логином уже существует'
    else:
        form = UserCreationForm()
    context['form'] = form
    return render(request, 'registration/signup.html', context)


def guide(request):
    return render(request, 'guide.html')


@login_required
def home(request):
    try:
        start_node = Node.objects.get(is_active=True, is_start=True)
    except Node.DoesNotExist:
        return HttpResponseBadRequest('Не найдена стартовая нода')
    start_node_user = NodeUser.objects.get(node=start_node, user=request.user)
    context = {'nodes': [[None, {
        'id': start_node.id,
        'status': start_node_user.status,
        'icon': start_node.icon.url,
        'is_start': start_node.is_start,
        'is_final': start_node.is_final,
    }, None]]}
    current_parent = start_node
    while not current_parent.is_final:
        level_nodes = Node.objects.filter(is_active=True, parent=current_parent)
        try:
            main_node = level_nodes.get(type='MAIN')
        except Node.DoesNotExist:
            return HttpResponseBadRequest('Проверьте структуру нод - правильно ли указан is_final')
        alt_nodes = level_nodes.filter(type='ALT').order_by('pk')
        if len(alt_nodes) != 2:
            return HttpResponseBadRequest('Проверьте структуру нод - везде ли по 2 альт-ноды')
        level_nodes = [alt_nodes[0], main_node, alt_nodes[1]]
        layer = []
        for level_node in level_nodes:
            level_node_user = NodeUser.objects.get(node=level_node, user=request.user)
            layer.append({
                'id': level_node.id,
                'status': level_node_user.status,
                'icon': level_node.icon.url,
                'is_start': level_node.is_start,
                'is_final': level_node.is_final,
            })
        context['nodes'].append(layer)
        current_parent = main_node
    return render(request, 'home.html', context)


@login_required
def leaderboard(request):
    context = {'users': []}
    users = User.objects.filter(is_staff=False, is_active=True)
    for user in users:
        done = NodeUser.objects.filter(user=user, status='DONE')
        finished = done.filter(node__is_final=True).count() > 0
        context['users'].append({
            'id': user.id,
            'username': user.username,
            'done': done.count(),
            'finished': finished,
        })
    context['users'] = sorted(context['users'], key=lambda x: (-x['finished'], -x['done'], x['username']))
    return render(request, 'leaderboard.html', context)


def get_node_redirect(node_status, node_id):
    if node_status == 'LOCKED':
        return redirect('locked_node', node_id=node_id, permanent=False)
    elif node_status == 'OPEN':
        return redirect('open_node', node_id=node_id, permanent=False)
    elif node_status == 'DONE':
        return redirect('done_node', node_id=node_id, permanent=False)
    else:
        return HttpResponseBadRequest('Wrong node status')


@login_required
def locked_node(request, node_id):
    _node = get_node(node_id)
    if not _node:
        raise Http404('Node does not exist')
    node_user = NodeUser.objects.get(user=request.user, node=_node)
    if node_user.status != 'LOCKED':
        return get_node_redirect(node_user.status, node_id)

    context = {
        'node': {
            'id': node_id,
            'type': _node.type,
            'is_start': _node.is_start,
            'is_final': _node.is_final,
            'icon': _node.icon.url
        }
    }

    if node_user.location_hint_1_open:
        context['node']['location_hint_1'] = _node.location_hint_1
    if node_user.location_hint_2_open:
        context['node']['location_hint_2'] = _node.location_hint_2

    context['level_nodes'] = get_level_nodes_header(_node)

    if request.method == 'POST':
        form = LockedNodeForm(request.POST)
        if form.is_valid():
            cleaned_data = form.cleaned_data
            password = _node.password.lower().strip()
            password_guess = cleaned_data["password_guess"].lower().strip()
            if password_guess == password:
                node_user.action_open()
                return redirect('open_node', node_id=node_id)
            else:
                context['error_msg'] = 'Неправильный код доступа'
    else:
        form = LockedNodeForm()

    context['form'] = form

    return render(request, 'nodes/locked_node.html', context)


@login_required
def open_node(request, node_id):
    _node = get_node(node_id)
    if not _node:
        raise Http404('Node does not exist')
    node_user = NodeUser.objects.get(user=request.user, node=_node)
    if node_user.status != 'OPEN':
        return get_node_redirect(node_user.status, node_id)

    context = {
        'node': {
            'id': node_id,
            'type': _node.type,
            'is_start': _node.is_start,
            'is_final': _node.is_final,
            'icon': _node.icon.url,
            'password': _node.password,
            'question': _node.question,
        }
    }

    if _node.image:
        context['node']['image'] = _node.image.url
    if node_user.location_hint_1_open:
        context['node']['location_hint_1'] = _node.location_hint_1
    if node_user.location_hint_2_open:
        context['node']['location_hint_2'] = _node.location_hint_2
    if node_user.question_hint_open:
        context['node']['question_hint'] = _node.question_hint

    context['level_nodes'] = get_level_nodes_header(_node)

    if request.method == 'POST':
        form = OpenNodeForm(request.POST)
        if form.is_valid():
            cleaned_data = form.cleaned_data
            answer_guess = cleaned_data.get("answer_guess").lower().strip()
            UserAnswerGuess(
                node_user=node_user,
                answer_guess=answer_guess,
                proposed=datetime.datetime.now()
            ).save()
            answers = [x.lower().strip() for x in Answer.objects.filter(node=_node).values_list('text', flat=True)]
            alt_error_msgs = dict(
                [(k.lower().strip(), v) for k, v in
                 AltErrorMsg.objects.filter(node=_node).values_list('answer_guess', 'error_msg')]
            )
            close_answer = False
            for answer in answers:
                if len(answer) == 4 and distance(answer, answer_guess) <= 1:
                    close_answer = True
                    break
                if len(answer) > 4 and distance(answer, answer_guess) <= 2:
                    close_answer = True
                    break
            if answer_guess in answers:
                node_user.action_done(answer_guess)
                return redirect('done_node', node_id=node_id)
            elif close_answer:
                context['error_msg'] = 'Этот ответ близко, проверьте на опечатки'
            elif answer_guess in alt_error_msgs:
                context['error_msg'] = alt_error_msgs[answer_guess]
            else:
                context['error_msg'] = 'Неправильный ответ'
    else:
        form = OpenNodeForm()

    context['form'] = form

    return render(request, 'nodes/open_node.html', context)


@login_required
def done_node(request, node_id):
    _node = get_node(node_id)
    if not _node:
        raise Http404('Node does not exist')
    node_user = NodeUser.objects.get(user=request.user, node=_node)
    if node_user.status != 'DONE':
        return get_node_redirect(node_user.status, node_id)

    context = {
        'node': {
            'id': node_id,
            'type': _node.type,
            'is_start': _node.is_start,
            'is_final': _node.is_final,
            'icon': _node.icon.url,
            'password': _node.password,
            'question': _node.question,
            'answer': node_user.picked_answer,
            'unlockable_text': _node.unlockable_text,
        }
    }

    if _node.image:
        context['node']['image'] = _node.image.url
    if node_user.location_hint_1_open:
        context['node']['location_hint_1'] = _node.location_hint_1
    if node_user.location_hint_2_open:
        context['node']['location_hint_2'] = _node.location_hint_2
    if node_user.question_hint_open:
        context['node']['question_hint'] = _node.question_hint

    context['level_nodes'] = get_level_nodes_header(_node)

    if not _node.is_final:
        if _node.type == 'MAIN':
            next_node = Node.objects.get(is_active=True, parent=_node, type='MAIN')
        else:
            next_node = Node.objects.filter(is_active=True, parent=_node.parent).get(type='MAIN')
        context['next_node'] = {
            'id': next_node.id,
            'icon': next_node.icon.url
        }

    return render(request, 'nodes/done_node.html', context)


@login_required
def node(request, node_id):
    _node = get_node(node_id)
    if not _node:
        raise Http404('Node does not exist')
    node_user = NodeUser.objects.get(user=request.user, node=_node)
    return get_node_redirect(node_user.status, node_id)
