from .models import Node, NodeUser, Answer, AltErrorMsg, UserAnswerGuess
from django.contrib import admin


admin.site.register(Node)
admin.site.register(Answer)
admin.site.register(AltErrorMsg)
admin.site.register(NodeUser)
admin.site.register(UserAnswerGuess)
