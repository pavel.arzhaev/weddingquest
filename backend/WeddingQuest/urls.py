"""WeddingQuest URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, re_path
from django.conf import settings
from django.views.generic import RedirectView
from django.views.static import serve
import os
from .views import signup, home, locked_node, open_node, done_node, node, leaderboard, login, logout, guide

urlpatterns = [
    path('', RedirectView.as_view(pattern_name='home', permanent=True), name='root'),
    path('login/', login, name='login'),
    path('logout/', logout, name='logout'),
    path('signup/', signup, name='signup'),
    path('guide/', guide, name='guide'),
    path('home/', home, name='home'),
    path('leaderboard/', leaderboard, name='leaderboard'),
    path('node/<int:node_id>/', node, name='node'),
    path('locked_node/<int:node_id>/', locked_node, name='locked_node'),
    path('open_node/<int:node_id>/', open_node, name='open_node'),
    path('done_node/<int:node_id>/', done_node, name='done_node'),
    path('admin/', admin.site.urls),
    path('favicon.ico', RedirectView.as_view(url='/static/favicon.ico', permanent=True)),
    re_path(r'^static/(?P<path>.*)$', serve, {'document_root': os.path.join(settings.BASE_DIR, "assets")}),
    re_path(r'^media/(?P<path>.*)$', serve, {'document_root': os.path.join(settings.BASE_DIR, "media")}),
]
