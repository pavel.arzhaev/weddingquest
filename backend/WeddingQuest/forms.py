from django.contrib.auth.models import User
from django import forms
from django.core.exceptions import ValidationError
from .models import Node


class LoginForm(forms.Form):
    username = forms.CharField(
        label='Логин', widget=forms.TextInput(attrs={'placeholder': 'Логин'})
    )
    password = forms.CharField(
        label='Пароль', widget=forms.PasswordInput(attrs={'placeholder': 'Пароль'})
    )


class UserCreationForm(forms.Form):
    username = forms.CharField(
        label='Логин',
        min_length=4,
        max_length=150,
        widget=forms.TextInput(attrs={'placeholder': 'Логин'})
    )
    password1 = forms.CharField(
        label='Пароль', widget=forms.PasswordInput(attrs={'placeholder': 'Пароль'})
    )
    password2 = forms.CharField(
        label='Пароль повторно', widget=forms.PasswordInput(attrs={'placeholder': 'Пароль повторно'})
    )


class LockedNodeForm(forms.Form):
    password_guess = forms.CharField(
        label='Код доступа', widget=forms.TextInput(attrs={'placeholder': 'Код доступа'})
    )


class OpenNodeForm(forms.Form):
    answer_guess = forms.CharField(
        label='Ответ', widget=forms.TextInput(attrs={'placeholder': 'Ответ'})
    )
