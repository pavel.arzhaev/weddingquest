import datetime

from django.db import models
from django.contrib.auth.models import User


class Node(models.Model):
    NODE_TYPE_CHOICES = (
        ("MAIN", "Главная"),
        ("ALT", "Альтернативная"),
    )

    type = models.CharField(
        max_length=100, choices=NODE_TYPE_CHOICES, default='MAIN', blank=False, null=False
    )
    parent = models.ForeignKey('self', on_delete=models.CASCADE, blank=True, null=True)
    icon = models.ImageField(upload_to='icons', blank=False, null=False)
    image = models.ImageField(upload_to='images', blank=True)
    location_hint_1 = models.TextField(blank=False, null=False)
    location_hint_2 = models.TextField(blank=True, null=True)
    password = models.CharField(max_length=100, blank=False, null=False)
    question = models.TextField(blank=False, null=False)
    question_hint = models.TextField(blank=True, null=True)
    unlockable_text = models.TextField(blank=False, null=False)
    is_start = models.BooleanField(blank=False, null=False, default=False)
    is_final = models.BooleanField(blank=False, null=False, default=False)
    is_active = models.BooleanField(blank=False, null=False, default=False)

    def save(self, *args, **kwargs):
        if self.pk:
            created = False
        else:
            created = True
        super(Node, self).save(*args, **kwargs)
        if created:
            for user in User.objects.all():
                node_user = NodeUser(
                    node=self,
                    user=user,
                )
                node_user.save()

    def __str__(self):
        return "{}{}".format(
            '●' if self.type == 'MAIN' else '',
            self.question[:50] if len(self.question) > 50 else self.question
        )

    class Meta:
        verbose_name = 'Node'
        verbose_name_plural = "Nodes"


class Answer(models.Model):
    node = models.ForeignKey(Node, on_delete=models.CASCADE, blank=False, null=False)
    text = models.CharField(max_length=100, blank=False, null=False)

    def __str__(self):
        return "{}{}: {}".format(
            '●' if self.node.type == 'MAIN' else '',
            self.node.question[:50] if len(self.node.question) > 50 else self.node.question,
            self.text
        )

    class Meta:
        verbose_name = 'Answer'
        verbose_name_plural = "Answers"


class AltErrorMsg(models.Model):
    node = models.ForeignKey(Node, on_delete=models.CASCADE, blank=False, null=False)
    answer_guess = models.CharField(max_length=100, blank=False, null=False, db_index=True)
    error_msg = models.TextField(blank=False, null=False)

    def __str__(self):
        return "{}{}: {}".format(
            '●' if self.node.type == 'MAIN' else '',
            self.node.question[:50] if len(self.node.question) > 50 else self.node.question,
            self.answer_guess
        )

    class Meta:
        verbose_name = 'AltErrorMsg'
        verbose_name_plural = "AltErrorMsgs"


class NodeUser(models.Model):
    NODE_STATUS_CHOICES = (
        ("LOCKED", "Доступа нет"),
        ("OPEN", "Пароль введен"),
        ("DONE", "Ответ на вопрос введен"),
    )

    node = models.ForeignKey(Node, on_delete=models.CASCADE, blank=False, null=False)
    user = models.ForeignKey(User, on_delete=models.CASCADE, blank=False, null=False)
    status = models.CharField(
        max_length=100, choices=NODE_STATUS_CHOICES, default='LOCKED', blank=False, null=False
    )
    open = models.DateTimeField(blank=True, null=True)
    done = models.DateTimeField(blank=True, null=True)
    location_hint_1_open = models.BooleanField(blank=False, null=False, default=False)
    location_hint_2_open = models.BooleanField(blank=False, null=False, default=False)
    question_hint_open = models.BooleanField(blank=False, null=False, default=False)
    picked_answer = models.CharField(max_length=100, blank=True, null=True)

    def action_open(self):
        self.status = 'OPEN'
        self.open = datetime.datetime.now()
        self.save()

    def action_done(self, picked_answer):
        self.status = 'DONE'
        self.done = datetime.datetime.now()
        self.picked_answer = picked_answer
        self.save()
        # Дадим подсказку на локацию на все наследные ноды
        if self.node.type == 'MAIN' and not self.node.is_final:
            next_level_nodes = Node.objects.filter(parent=self.node, is_active=True)
            for level_node in next_level_nodes:
                level_node_user = NodeUser.objects.get(node=level_node, user=self.user)
                level_node_user.location_hint_1_open = True
                level_node_user.save()
        if self.node.type == 'ALT':
            level_nodes = Node.objects.filter(parent=self.node.parent, is_active=True)
            main_node = level_nodes.get(type='MAIN')
            alt_nodes = level_nodes.filter(type='ALT')
            done_alts = 0
            for alt_node in alt_nodes:
                if NodeUser.objects.get(node=alt_node, user=self.user).status == 'DONE':
                    done_alts += 1
            if done_alts > 0:
                main_node_user = NodeUser.objects.get(node=main_node, user=self.user)
            # Дадим подсказки на главную ноду
            if done_alts == 1:
                main_node_user.location_hint_2_open = True
                main_node_user.question_hint_open = True
                main_node_user.save()
            # Решим главную ноду
            if done_alts > 1:
                main_node_user.action_done(Answer.objects.filter(node=main_node).first().text)

    def __str__(self):
        return '({}): {}{}'.format(
            self.user.username,
            '●' if self.node.type == 'MAIN' else '',
            self.node.question[:50] if len(self.node.question) > 50 else self.node.question,
        )

    class Meta:
        verbose_name = 'Node <-> User'
        verbose_name_plural = "Node <-> User connections"


class UserAnswerGuess(models.Model):
    node_user = models.ForeignKey(NodeUser, on_delete=models.CASCADE, blank=False, null=False)
    answer_guess = models.TextField(blank=True, null=True)
    proposed = models.DateTimeField(blank=False, null=False)

    def __str__(self):
        return '({}, {}{}): {}'.format(
            self.node_user.user.username,
            '●' if self.node_user.node.type == 'MAIN' else '',
            self.node_user.node.question[:50] if len(self.node_user.node.question) > 50 else self.node_user.node.question,
            self.answer_guess
        )

    class Meta:
        verbose_name = 'UserAnswerGuess'
        verbose_name_plural = "UserAnswerGuesses"
